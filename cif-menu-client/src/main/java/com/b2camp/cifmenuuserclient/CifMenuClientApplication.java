package com.b2camp.cifmenuuserclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class CifMenuClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(CifMenuClientApplication.class, args);
	}

}
