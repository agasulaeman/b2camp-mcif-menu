package com.b2camp.cifmenuserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class CifMenuServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CifMenuServerApplication.class, args);
	}

}
